const chai = require('chai');;
const chaiHttp = require('chai-http');
const expect = require('chai').expect;


const route = '/api/vi/quote/car-insurance';
const baseUrl = 'http://localhost:8080';

chai.use(chaiHttp);

describe('Success and Status', () => {
    it('should return success:false and status 400 if request is made invalid values', function () {
        chai.request(baseUrl).post(route).send({
            car_value: 3000.0,
            driver_birthdate: '29/03/20999'
        })
        .end((err, res) => {
            // console.log(err)
            expect(res.body.success).to.be.false
            expect(res).to.have.status(400)
        done
        })
    })

    it('should return success:true and status 200 if the request is made with valid values', function () {
        chai.request(baseUrl).post(route).send({
                car_value: 3000.0,
                driver_birthdate: '29/03/2000'
        })
        .end((err, res) => {
            // console.log(err)
            expect(res.body.success).to.be.true
            expect(res).to.have.status(200)
        })
    })
})