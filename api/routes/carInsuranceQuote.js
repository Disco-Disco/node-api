const express = require('express');
const { check } = require('express-validator');
const moment = require('moment');

const router = express.Router();

const responseController = require('../controllers/response');

router.post(
    '/car-insurance',
    [
        // I'm pressuming that people who's car is worthless can still apply for a quote..?
        check('car_value').isFloat({ min: 0.0 }),

        // check valid date format 
        check('driver_birthdate')
            .custom((value, { req }) => {
            
            if (!moment(value, 'DD/MM/YYYY').isValid()) {
                throw new Error("Either that year was not a leap year, or invalid date format. Use 'DD.MM.YYYY' or 'DD/MM/YYYY' or 'DD-MM-YYY'")
            }
            return true
        }),
        
        // Not being in the future. If in future, moment().diff(,,) returns NaN
        check('driver_birthdate')
            .custom((value, { req }) => {
            // second argument of diff's separators ('/' or '-' r '.') dont seem to matter
            if (!moment(value, 'DD/MM/YYYY').isBefore()) {
                throw new Error('driver_birthdate cannot be in the future')
            }
            return true;
        })
    ],
    responseController.postResponse);

module.exports = router;
