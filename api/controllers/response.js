const { validationResult } = require('express-validator');
const moment = require('moment');

const getDriverAge = (date) => {
    // .diff also truncates decimal to 0 places instead of rounding
    return moment().diff(moment(date, 'DD/MM/YYYY'), 'years')
}

exports.postResponse = (req, res) => {
    const errors = validationResult(req)

    const carValue = req.body.car_value;
    const driverBirthdate = req.body.driver_birthdate;
    
    console.log(errors.array());
    
    if (!errors.isEmpty()) {
        const errorMessages = errors.array().map(error => error.msg);
        return res.status(400).json({
            success: false,
            message: "parameters missing or incorrect values",
            errorSpecifics: errorMessages
        })
    }


    const driverAge = getDriverAge(driverBirthdate);
    
    if (driverAge >= 18) {
        res.status(200).json({
            success: true,
            message: "quote successfully computed",
            data: {
                eligible: true,
                premiums: {
                    civil_liability:  driverAge >= 26 ? 500 : 1000,
                    omnium: Math.round((carValue * 0.03 + Number.EPSILON) * 100) / 100,
                    // Rounds to second decimal properly but with scaling: What we could do here is
                    // bring two figures across the decimal point, turning the figure into a whole
                    // number to avoid all the crazy floating point issues, round that and then 
                    // translate it back into what it was before by dividing by 100 and you have the number
                    // rounded to 2 decimal points
                }
            }
        });
    }
    else {
        res.status(200).json({
            success: true,
            message: "quote successfully computed",
            data: {
                eligible: false,
                premiums: null
            }
        })
    }
};

