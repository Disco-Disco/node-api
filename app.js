const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const carInsuranceQuoteRoutes = require('./api/routes/carInsuranceQuote');

app.use((req, res, next) => {
    req.headers["content-type"] = "application/json";
    next();
});

app.use(bodyParser.json());

app.use('/api/v1/quote', carInsuranceQuoteRoutes);

module.exports = app;